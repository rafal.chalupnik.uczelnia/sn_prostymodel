import math
import matplotlib.pyplot as plt
import numpy as np
import sys
import Utils


class Adaline:
    def __init__(self, input_count, random_scope = 1.0, activation_bias = 0.0, bipolar = True, constant = 1):
        self.weights = random_scope * (2 * np.random.rand(input_count + 1) - 1.0)
        self.activation_bias = activation_bias
        self.bipolar = bipolar
        self.constant = constant

    def compute(self, input, activate_neuron = True):
        input = np.append(input, self.constant)
        multiplied = np.dot(input, self.weights)

        if activate_neuron:
            return Utils.activation_function(multiplied, self.activation_bias, self.bipolar)
        else:
            return multiplied

    def train(self, train_inputs, train_outputs, learn_rate, accepted_error):
        error = sys.float_info.max
        train_length = len(train_inputs)

        while error > accepted_error:
            error = 0

            for i in range(train_length):
                input = np.array(train_inputs[i])
                computed = self.compute(input, False)
                error += math.pow(train_outputs[i] - computed, 2)

            error /= train_length

            if error > accepted_error:
                for j in range(train_length):
                    computed = self.compute(train_inputs[j], False)
                    input = np.append(train_inputs[j], self.constant)
                    self.weights += learn_rate * (train_outputs[j] - computed) * input

            print self.weights
