import time
from Adaline import Adaline
from Perceptron import Perceptron
import numpy as np


accepted_error = 0.3
activation_bias = 0.5
bipolar = True
constant = 1
learn_rate = 0.2
random_weight_scope = 0.3


train_input = [[-1, -1], [-1, 1], [1, -1], [1, 1]]
train_output = [-1, -1, -1, 1]

# train_input = [[0, 0], [0, 1], [1, 0], [1, 1]]
# train_output = [0, 0, 0, 1]


if __name__ == '__main__':
    sum = 0

    for i in range(50):
        start_time = time.clock()

        # neuron = Perceptron(2, random_weight_scope, activation_bias, bipolar)
        # neuron.train(train_input, train_output, learn_rate)

        neuron = Adaline(2, random_weight_scope, activation_bias, bipolar, constant)
        neuron.train(train_input, train_output, learn_rate, accepted_error)

        stop_time = time.clock()

        sum += stop_time - start_time

    sum /= 50

    print "Average time:"
    print sum
