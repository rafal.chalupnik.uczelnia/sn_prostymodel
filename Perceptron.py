import numpy as np
import Utils


class Perceptron:
    def __init__(self, input_count, random_scope = 1.0, activation_bias = 0.5, bipolar = False):
        self.weights = Utils.generate_random_weights(input_count, random_scope)
        self.activation_bias = activation_bias
        self.bipolar = bipolar

    def compute(self, input):
        multiplied = np.dot(input, self.weights)
        return Utils.activation_function(multiplied, self.activation_bias, self.bipolar)

    def train(self, train_inputs, train_outputs, learn_rate):
        all_correct = False
        train_length = len(train_inputs)

        while not all_correct:
            all_correct = True

            for i in range(train_length):
                input = np.array(train_inputs[i])
                computed = self.compute(input)
                error = train_outputs[i] - computed

                if error != 0:
                    all_correct = False
                    self.weights += learn_rate * error * input

            print self.weights
