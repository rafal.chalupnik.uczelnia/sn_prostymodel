import numpy as np


def activation_function(input, activation_bias, bipolar):
    if input >= activation_bias:
        return 1
    else:
        if bipolar:
            return -1
        else:
            return 0


def generate_random_weights(input_length, random_scope):
    return random_scope * (2 * np.random.rand(input_length) - 1.0)
